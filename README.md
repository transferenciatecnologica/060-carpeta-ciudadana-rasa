# Manual técnico 060 bot
## Diagrama arquitectura
![](images/060Chatbot.png)
## Funcionamiento
Como se puede observar en el diagrama del sistema, se obtienen los modelos de reconocimiento del lenguaje natural en una máquina de desarrollo en la que está desplegada una aplicación web interactiva (RasaX) que será gestionada por el departamento de comunicación. 
Estos modelos serán cargados en los distintos despliegues de los bots realizados mediante docker, tanto en los entornos de preproducción como en producción una vez confirmado su correcto funcionamiento. Cada uno de estos bots consumirá a su vez un servidor de acciones (operaciones que alteran el comportamiento de los bots externas al entrenamiento) que se levantará en la misma máquina.
Además de estos bots, se desplegará un servicio REST (pasarela 060-endpoint) que actúa como conector entre CCX y nuestros bots, enviando las peticiones a los mismos y generando los JSON de respuesta esperados.
## Tecnologías utilizadas
### Rasa
Rasa es una librería de código abierto que utilizaremos para construir nuestro asistente virtual (AI Chatbot). La mayor parte de entrenamientos y configuración se realiza alterando distintos ficheros .yaml y .md que describiremos en posteriores apartados. Utilizaremos la versión 2.3.4 de rasa para todas nuestras máquinas.
### Python
Python es el lenguaje utilizado para desarrollar tanto la pasarela de conexión (060-endpoint) como las distintas operaciones que alteran el comportamiento de nuestro bot (acciones de rasa). Se ha utilizado la versión 3.6.8 para el desarrollo de este sistema.
### Docker
Utilizaremos docker-compose para automatizar el despliegue de los distintos servicios desplegados, es decir, levantar simultáneamente los bots de rasa, el servidor de acciones y la pasarela de conexión con CCX.  Se ha utilizado la versión 19.03.12 de Docker Engine - Community.

## Servicios desplegados
Este sistema únicamente ofrece un servicio en cada una de los entornos que utilizaremos (desarrollo, preproducción y producción).
* Desarrollo: Se levanta una aplicación web que permite realizar entrenamientos, comprobar el funcionamiento del bot o interactuar con él. Actualmente, se encuentra desplegado en [20.56.121.134:5002/login](20.56.121.134:5002/login)
*  Preproducción: El sistema en preproducción tiene una estructura ligeramente distinta al despliegue final que llevaremos a cabo en producción, ya que únicamente utilizamos una máquina con dos bots desplegados sobre distintos puertos. 
Cada uno de estos bots ofrece un servicio REST sobre el que podemos realizar peticiones directamente. Sin embargo, únicamente nos comunicaremos con estos bots mediante la pasarela 060-endpoint, desplegada en la misma máquina, la cual amplía la respuesta añadiendo las cabeceras necesarias antes de devolverla al CCX. Para decidir con qué bot queremos interactuar en este sistema, se indicará en la propia petición mediante la url /talk o /talk2 respectivamente:
	+ 		+	curl -XPOST http://172.17.0.1:9000/talk -d '{"sender":"001", "message":"pedir cita para el DNI"}' -H "Content-Type: application/json" | python3 -m json.tool
	+		+	curl -XPOST http://172.17.0.1:9000/talk2 -d '{"sender":"001", "message":"pedir cita para el DNI"}' -H "Content-Type: application/json" | python3 -m json.tool
* Producción: El sistema de producción lleva la redundancia de los bots un paso más allá, replicando los bots una vez más mediante el uso de tres máquinas distintas. 
Existe una máquina que actúa como controlador a la que se realizan las peticiones (nginx, balanceo de carga) que se redirigirán a cualquiera de las otras dos máquinas (despliegue bots). En cada una de estas dos máquinas podremos encontrar dos bots desplegados mediante docker, situados detrás de una pasarela con comportamiento similar al sistema de preproducción. Sim embargo, cada una de estas pasarelas se deberá comunicar únicamente con un bot, es decir, habrá cuatro pasarelas 060-endpoint que utilizaremos para realizar las peticiones a cada uno de estos bots.
Máquina uno:
	+ 		+	curl -XPOST http://10.201.195.47:9001/talk -d '{"sender":"001", "message":"pedir cita para el DNI"}' -H "Content-Type: application/json" | python3 -m json.tool
	+ 		+	curl -XPOST http://10.201.195.47:9002/talk -d '{"sender":"001", "message":"pedir cita para el DNI"}' -H "Content-Type: application/json" | python3 -m json.tool
	Máquina dos:
	+ 		+	curl -XPOST http://10.201.195.48:9001/talk -d '{"sender":"001", "message":"pedir cita para el DNI"}' -H "Content-Type: application/json" | python3 -m json.tool
	+ 		+	curl -XPOST http://10.201.195.48:9002/talk -d '{"sender":"001", "message":"pedir cita para el DNI"}' -H "Content-Type: application/json" | python3 -m json.tool
  
## Entornos de trabajo
### Desarrollo
En este entorno se puede trabajar de dos maneras distintas, utilizando la aplicación web y gestionando los distintos ficheros a mano. Teniendo esto en cuenta, debemos conocer la estructura de ficheros de rasa y qué debemos modificar en cada momento.
#### Estructura y ficheros

* /data - En esta carpeta se almacenan dos de los ficheros más importantes, relacionados con el entrenamiento del bot:

* nlu.md: Contiene los datos de entrenamiento, es decir, qué frases se corresponden con cada "intent" (interpretación de una entrada del usuario) reconocido por nuestro chatbot.

* stories.md: Contiene los distintos casos de uso del bot, es decir, interacciones completas del usuario con nuestro chatbot.

* Config.yml - En este fichero se realiza la configuración de los distintos elementos que utiliza nuestro bot. Únicamente nos interesan dos parámetros, nlu_threshold y core_threshold. Nuestro chatbot tendrá más tolerancia ante entradas que no se corresponden con ninguna frase del entrenamiento cuanto menor sea el valor de estos parámetros (0.3 por defecto).

* Domain.yml - Fichero que contiene los utters, es decir, respuestas del bot. Además también se especifican los intents que reconoce nuestro chatbot.

* Actions.py - Acciones personalizadas que se utilizan en la ejecución del bot. En nuestro caso, hemos configurado dos:

* ActionRephrase: Se invoca cuando el usuario introduce un texto que nuestro chatbot no puede interpretar como ninguno de los intents introducidos. Pedirá al usuario repetir.

* ActionPasarAgente: Se invoca cuando el usuario se ha equivocado dos veces, y devuelve una respuesta a la vez que un código que se interpretará en el conector intermedio entre rasa y ccx.

### Ejemplo de uso

#### Rasa Shell

Para levantar el servicio de manera local y poder utilizar la propia terminal para realizar pruebas con nuestro chatbot, debemos ejecutar las siguientes instrucciones en la carpeta donde se haya clonado el repositorio una vez instalada la versión de rasa indicada previamente.

~~~~

rasa run actions

rasa shell

~~~~

#### Rasa Server

En caso de que queramos desplegar el chatbot y mantenerlo accesible, además de permitir peticiones http al NLU, lo haremos de la siguiente manera:

~~~~

rasa run actions

rasa run --enable-api

~~~~

  

#### Rasa X

Para levantar la interfaz gráfica de rasa X, debemos seguir los pasos indicados en el manual de instalación [Rasa X docker-compose](https://rasa.com/docs/rasa-x/installation-and-setup/install/docker-compose). Una vez realizados estos pasos, podremos levantar nuestra aplicación mediante la siguiente instrucción.
~~~~

docker-compose up -d

~~~~
Esta aplicación estará disponible en la URL que nos indique al ejecutar el comando, configurable desde el propio docker-compose.

Para detener el servicio, ejecutaremos:
~~~~~

docker-compose down

~~~~~
### Preproducción
Este entorno se utilizará para realizar pruebas de funcionamiento con los modelos generados en desarrollo, por tanto, no será necesario instalar la aplicación Web RasaX. Únicamente deberemos clonar el repositorio y levantar ambos bots (/talk y /talk2) y la pasarela que permite la comunicación entre el CCX y los mismos:
Actualmente, se realiza el despliegue por separado aunque se plantea integrar todo en un mismo dockerfile. Existe un script de despliegue (start.sh) en /data/rasa que despliega ambos bots de la siguiente manera:

    cd /data/rasa/060-carpeta-ciudadana-rasa
        
    docker run -d -v $(pwd):/app -p 127.0.0.1:5000:5005 --restart=unless-stopped --name rasa_060-carpeta-ciudadana rasa/rasa:2.3.4-full run --enable-api --debug
        
    docker exec -it rasa_060-carpeta-ciudadana /bin/bash
        
    rasa run actions -p 5055
        
    exit
        
    cd /data/rasa/060-carpeta-ciudadana-rasa2
        
    docker run -d -v $(pwd):/app -p 127.0.0.1:5006:5005 --restart=unless-stopped --name rasa_060-carpeta-ciudadana2 rasa/rasa:2.3.4-full run --enable-api --debug
        
     docker exec -it rasa_060-carpeta-ciudadana2 /bin/bash
        
      rasa run actions -p 5055
        
      exit
La pasarela que utilizamos como conector se encuentra en /data/rasa/060-endpoint/src
Para su correcto despliegue, los requisitos almacenados en requirements.txt deberán estar instalados. Esto se podrá realizar mediante:

    pip install -r requirements.txt
Una vez se cumplan todos los requisitos, podremos ejecutar el script start.sh en la misma carpeta, que levantará la pasarela de la siguiente manera:

    gunicorn --access-logfile ../logs/access.log --log-file ../logs/gunicorn.log --reload --daemon --workers 1 --threads 2 -b 0.0.0.0:9000 wsgi
Tras haber desplegado tanto los bots como la pasarela, podremos comprobar su funcionamiento haciendo peticiones a la url de la pasarela, indicando que queremos hablar con cualquiera de los dos bots de la siguiente manera:

	curl -XPOST http://172.17.0.1:9000/talk -d '{"sender":"001", "message":"pedir cita para el DNI"}' -H "Content-Type: application/json" | python3 -m json.tool
	curl -XPOST http://172.17.0.1:9000/talk2 -d '{"sender":"001", "message":"pedir cita para el DNI"}' -H "Content-Type: application/json" | python3 -m json.tool
También se puede comprobar que los bots están disponibles utilizando el comando `docker ps`	o comprobando los puertos que están escuchando mediante `sudo lsof -i -P -n | grep LISTEN`
### Producción
Para preparar el entorno de producción de acuerdo a la arquitectura mostrada en el primer apartado, debemos asegurarnos de que nuestras máquinas cuentan con las versiones de Python y Docker adecuadas (apartado Tecnologías).
Una vez instaladas las herramientas necesarias, clonaremos los repositorios de los bots y de sus pasarelas correspondientes en las carpetas en las que queramos realizar la instalación. Para este ejemplo utilizaremos /data/rasa como directorio principal. Dentro de esta carpeta crearemos dos subdirectorios, /data/rasa/060bot y /data/rasa/060pruebas

```
mkdir 060bot

mkdir 060pruebas

cd 060bot

git clone -b 060bot https://gitlab.com/transferenciatecnologica/060-endpoint

git clone -b 060bot https://gitlab.com/transferenciatecnologica/060-carpeta-ciudadana-rasa

cd ../060pruebas

git clone -b 060pruebas https://gitlab.com/transferenciatecnologica/060-endpoint

git clone -b 060pruebas https://gitlab.com/transferenciatecnologica/060-carpeta-ciudadana-rasa
```


#### Bot principal (060bot)
Para el primer despliegue, nos dirigiremos al directorio 060-carpeta-ciudadana y ejecutaremos el script start.sh, que se encargará de levantar tanto el bot como el servidor de acciones:
```
cd /data/rasa/060bot/060-carpeta-ciudadana
sh start.sh
```


Una vez hecho esto, levantaremos la pasarela correspondiente. Para ello, nos dirigimos a la carpeta /data/rasa/060bot/060-endpoint/src e instalamos las dependencias antes de ejecutar el script de despliegue:
```
cd /data/rasa/060bot/060-endpoint/src
pip install -r requirements.txt
sh start.sh
```



Una vez levantada la pasarela, nos aseguramos de que está escuchando en el puerto correspondiente (9001) ejecutando el siguiente comando:

`lsof -i -P -n | grep LISTEN`




Tras levantar este bot y su pasarela correspondiente, podremos realizar peticiones al mismo de la siguiente manera:
`  curl -XPOST http://IP_MÁQUINA_1o2:9001/talk -d '{"sender":"001", "message":"pedir cita para el DNI"}' -H "Content-Type: application/json" | python3 -m json.tool`

#### Bot de pruebas (060pruebas)
Por último, levantaremos el segundo bot y su pasarela replicando estas instrucciones en la carpeta 060pruebas que hemos creado anteriormente:

```
cd /data/rasa/060pruebas/060-carpeta-ciudadana
sh start.sh
```


Una vez hecho esto, levantaremos la pasarela correspondiente. Para ello, nos dirigimos a la carpeta /data/rasa/060pruebas/060-endpoint/src e instalamos las dependencias antes de ejecutar el script de despliegue:
```
cd /data/rasa/060pruebas/060-endpoint/src
pip install -r requirements.txt
sh start.sh
```


Una vez levantada la pasarela, nos aseguramos de que está escuchando en el puerto correspondiente (9002) ejecutando el siguiente comando:

`lsof -i -P -n | grep LISTEN`



Tras levantar este bot y su pasarela correspondiente, podremos realizar peticiones al mismo de la siguiente manera:
`  curl -XPOST http://IP_MÁQUINA_1o2:9002/talk -d '{"sender":"001", "message":"pedir cita para el DNI"}' -H "Content-Type: application/json" | python3 -m json.tool`


# Manual de operaciones 060 bot
## Entorno tecnológico 
La tecnología utilizada para desarrollar y poner en funcionamiento nuestros bots puede ser desplegada sobre los siguientes sistemas operativos:

*  Ubuntu 16.04 / 18.04
    
*  Debian 9 / 10
    
*  Red Hat 7 / 8
    
*  CentOS 7 / 8

Los requisitos mínimos indicados pasa ejecutar tanto Rasa como RasaX serían los siguientes:
**vCPUs**

-   Mínimo: 2 vCPUs
    
-   Recomendado: 2-6 vCPUs
    

**RAM**

-   Mínimo: 4 GB RAM
    
-   Recomendado: 8 GB RAM
    

**Disk Space**

-   Recomendado: 50 GB de espacio de disco disponible

## Proceso de actualización
A medida que vamos realizando cambios y mejoras en nuestros modelos entrenados en la máquina de desarrollo, querremos introducirlos en los sistemas de preproducción para realizar las pruebas pertinentes, y, si estas pruebas son satisfactorias, pasarlos a producción interrumpiendo el funcionamiento del sistema el menor tiempo posible.
Para cumplir este objetivo, utilizaremos un método de la API Http de rasa que nos permite cambiar el modelo cargado en la máquina en runtime. 
Simplemente debemos copiar el fichero del modelo que queramos introducir dentro de los distintos contenedores de los bots mediante `docker cp` y realizar una petición [PUT de reemplazo de modelo](https://rasa.com/docs/rasa/pages/http-api#operation/replaceModel) a la API como podemos ver a continuación:

    curl -v XPUT http://localhost:5006/model -d '{"model_file":"/app/models/20210408-092054.tar.gz"}' -H "Content-Type: application/json"
Una vez realizado este cambio, el sistema comenzará a utilizar el nuevo modelo para las nuevas conversaciones. 
## Backups
El código de todas las partes programables, es decir, las acciones de rasa, los ficheros de configuración y la implementación de la pasarela 060-endpoint está almacenado en los siguientes gitlabs:
* https://gitlab.com/transferenciatecnologica/060-carpeta-ciudadana-rasa
* https://gitlab.com/transferenciatecnologica/060-endpoint

Sin embargo, los modelos que se utilizan únicamente se encuentran almacenados en los entornos de desarrollo, preproducción y producción. Sería importante realizar copias de seguridad de los modelos hasta una determinada antigüedad, en caso de que sea necesario recuperarlos y las máquinas necesarias no estuviesen disponibles. 
## Sistema de logs
### Rasa
Los logs de rasa se pueden comprobar mediante la instrucción `docker logs` del contenedor que queramos consultar. Esto sería necesario en caso de que uno de los bots tuviese un funcionamiento no esperado y quisiéramos realizar un seguimiento del comportamiento, analizando las respuestas y los intervalos de confianza que devuelve en cada paso. En el caso de producción, estos logs se pueden comprobar mediante el comando `docker compose logs` seguido del nombre del servicio que queramos monitorizar (ya sean las acciones, el bot en sí o la pasarela).
### Pasarela 060-endpoint
Esta pasarela almacena los logs de manera independiente en la carpeta /data/rasa/060-endpoint/logs para la máquina de preproducción. En ella, podemos encontrar varios ficheros:
* access.log: Almacena la fecha, tipo de petición, usuario que la realiza y código devuelto (200, 404...).
* app.log: Almacena la fecha, ID del usuario (identificador único para cada conversación), intent devuelto por el bot y valor devuelto para la confianza (en el intervalo 0-1).
* gunicorn.log: Almacena los logs del gunicorn utilizado para el despliegue de esta pasarela en caso de error.
