

import os
import json

directory = 'dialogflow/intents'

lista = []

for filename in os.listdir(directory):
    if not filename.endswith("usersays_es.json"):
        # print("\n*******Nuevo:")
        # print(filename)
        # print(filename.split(".")[0])

        with open(os.path.join(directory, filename)) as f:
            data = json.load(f)
            lista.append(data['name'])

            

            # if('speech' in data['responses'][0]["messages"][0]):
            #     print(data['responses'][0]["messages"][0]['speech'])
            # else:
            #     try:
            #         print(data['responses'][0]["messages"][1]['speech'])
            #     except IndexError:
            #         print('Caso especial, no sigue la estructura o no tiene utter')

lista.sort()
for i in lista:
    print("\n## " + i)
    print("* " + i)
    print("  - " + "utter_" + i)