from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

from rasa_sdk.events import *

class ActionPasarAgenteOutOfScope(Action):

    def name(self) -> Text:
        return "action_pasar_agente_outofscope"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # dispatcher.utter_message(template='')
        dispatcher.utter_message(text="Lo siento. No estoy entrenado para resolver tu consulta. Te transfiero con un agente para que te pueda ayudar. Muchas gracias por utilizar este servicio.")
        dispatcher.utter_custom_json({"final":{"tipo":"agente","skills":["agente"]}})
        #dispatcher.utter_message(text="Clave agente")

        return []


class ActionPasarAgente(Action):

    def name(self) -> Text:
        return "action_pasar_agente"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # dispatcher.utter_message(template='')
        dispatcher.utter_message(text="Lo siento. No estoy entrenado para resolver tu consulta. Te transfiero con un agente para que te pueda ayudar. Muchas gracias por utilizar este servicio.")
        dispatcher.utter_custom_json({"final":{"tipo":"agente","skills":["agente"]}})
        #dispatcher.utter_message(text="Clave agente")

        return [UserUtteranceReverted()]

class ActionDefaultAskRephrase(Action):
    """Asks for an affirmation of the intent if NLU threshold is not met."""

    def name(self):
       return "action_default_ask_rephrase"

    def run(self, dispatcher, tracker, domain):
        dispatcher.utter_message(text='Prueba action rephrase')
        return []

class ActionDefaultAskAffirmation(Action):
   """Asks for an affirmation of the intent if NLU threshold is not met."""

   def name(self):
       return "action_default_ask_affirmation"

   def run(self, dispatcher, tracker, domain):
        #dispatcher.utter_message(template = '')
        dispatcher.utter_message(text="Disculpa, no te he entendido. Quizás no esté suficientemente entrenado para ayudarte pero ¿puedes explicarme de otra manera qué necesitas?")
        return []


class ActionCierreResolucion(Action):

    def name(self) -> Text:
        return "action_cierre_resolucion"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(template='utter_cierre_resolucion')
        dispatcher.utter_custom_json({"final":{"tipo":"cierre_conversacion"}})

        return []
