

import os
import json

directory = 'dialogflow/intents'

c = 0

for filename in os.listdir(directory):
    if not filename.endswith("usersays_es.json"):
        print("\n*******Nuevo:")
        print(filename)
        # print(filename.split(".")[0])

        with open(os.path.join(directory, filename)) as f:
            data = json.load(f)
            print(data['name'])
            if('speech' in data['responses'][0]["messages"][0]):
                print(data['responses'][0]["messages"][0]['speech'])
                c = c+1
            else:
                try:
                    print(data['responses'][0]["messages"][1]['speech'])
                    c = c+1
                except IndexError:
                    print('Caso especial, no sigue la estructura o no tiene utter')
        print(c)

