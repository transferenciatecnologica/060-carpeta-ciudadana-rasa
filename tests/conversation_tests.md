## ## This file contains tests to evaluate that your bot behaves as expected.

## ## If you want to learn more, please see the docs: https://rasa.com/docs/rasa/user-guide/testing-your-assistant/

## happy path 1
* greet: hello there!
  - utter_greet
* mood_great: amazing
  - utter_happy

## happy path 2
* greet: hello there!
  - utter_greet
* mood_great: amazing
  - utter_happy
* goodbye: bye-bye!
  - utter_goodbye

## sad path 1
* greet: hello
  - utter_greet
* mood_unhappy: not good
  - utter_cheer_up
  - utter_did_that_help
* affirm: yes
  - utter_happy

## sad path 2
* greet: hello
  - utter_greet
* mood_unhappy: not good
  - utter_cheer_up
  - utter_did_that_help
* deny: not really
  - utter_goodbye

## sad path 3
* greet: hi
  - utter_greet
* mood_unhappy: very terrible
  - utter_cheer_up
  - utter_did_that_help
* deny: no
  - utter_goodbye

## say goodbye
* goodbye: bye-bye!
  - utter_goodbye

## bot challenge
* bot_challenge: are you a bot?
  - utter_iamabot

## New Story

* clave_info: Necesito una clave para sacar unos papeles pero no tengo claro qué es ni cómo conseguirla
    - utter_clave_info

## New Story

* clave_error_generico: Hola tengo un problema con la clave
    - utter_clave_error_generico
* clave_error_nivel_registro: estoy en la página del SEPE y me dice que no me registré presencialmente
    - utter_clave_error_nivel_registro
* clave_error_confirma_oficina: Entonces si no tengo certificado digital ni dni electrónico tengo que ir a una oficina
    - utter_clave_error_confirma_oficina
